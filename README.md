# Routing project

To download this code, execute in bash:

```bash
git clone git@bitbucket.org:agcastro7/routing-project.git
cd routing-project
bundle
rails db:migrate
rails s
```
